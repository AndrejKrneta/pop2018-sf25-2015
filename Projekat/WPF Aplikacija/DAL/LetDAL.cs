﻿using ConsoleAppFirst.Model;
using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DrugiWPF.DAL
{
    public class LetDAL
    {
        public static void UcitajLetove()
        {
            Aplikacija.Instance.Letovi.Clear();

            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM LET";

                SqlDataAdapter daAdapter = new SqlDataAdapter();
                DataSet daLet = new DataSet();

                daAdapter.SelectCommand = command;
                daAdapter.Fill(daLet, "LET");

                foreach (DataRow row in daLet.Tables["LET"].Rows)
                {
                    Let let = new Let();
                    {
                        let.ID = (int)row["id"];
                        let.Sifra = (int)row["sifra"];
                        let.Odrediste = (string)row["odrediste"];
                        let.Destinacija = (string)row["destinacija"];
                        let.VremePolaska = (DateTime)row["vremepolaska"];
                        let.VremeDolaska = (DateTime)row["vremedolaska"];
                        let.CenaKarte = (int)row["cenakarte"];
                        let.Obrisano = (bool)row["obrisano"];

                        Aplikacija.Instance.Letovi.Add(let);
                    }
                }
            }
        }

        public static void DodajLet(Let l)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO LET (sifra, odrediste, destinacija, vremepolaska, vremedolaska, cenakarte, obrisano) 
                                        VALUES (@sifra, @odrediste, @destinacija, @vremepolaska, @vremedolaska, @cenakarte, @obrisano)";

                command.Parameters.Add(new SqlParameter("@sifra", l.Sifra));
                command.Parameters.Add(new SqlParameter("@odrediste", l.Odrediste));
                command.Parameters.Add(new SqlParameter("@destinacija", l.Destinacija));
                command.Parameters.Add(new SqlParameter("@vremepolaska", l.VremePolaska));
                command.Parameters.Add(new SqlParameter("@vremedolaska", l.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@cenakarte", l.CenaKarte));
                command.Parameters.Add(new SqlParameter("@obrisano", l.Obrisano));

                command.ExecuteNonQuery();
            }
        }
        public static void IzmenaLeta(Let l)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE LET 
                                        SET sifra=@sifra, odrediste=@odrediste, destinacija=@destinacija, vremepolaska=@vremepolaska, 
                                            vremedolaska=@vremedolaska, cenakarte=@cenakarte, obrisano=@obrisano WHERE id=@id";

                command.Parameters.Add(new SqlParameter("@id", l.ID));
                command.Parameters.Add(new SqlParameter("@sifra", l.Sifra));
                command.Parameters.Add(new SqlParameter("@odrediste", l.Odrediste));
                command.Parameters.Add(new SqlParameter("@destinacija", l.Destinacija));
                command.Parameters.Add(new SqlParameter("@vremepolaska", l.VremePolaska));
                command.Parameters.Add(new SqlParameter("@vremedolaska", l.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@cenakarte", l.CenaKarte));
                command.Parameters.Add(new SqlParameter("@obrisano", l.Obrisano));

                command.ExecuteNonQuery();
            }
        }

        public static void IzbrisiLet(Let l)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE LET SET obrisano=@obrisano WHERE ID=@id";

                command.Parameters.Add(new SqlParameter("@id", l.ID));
                command.Parameters.Add(new SqlParameter("@obrisano", 1));

                command.ExecuteNonQuery();
            }
        }

    }
}
