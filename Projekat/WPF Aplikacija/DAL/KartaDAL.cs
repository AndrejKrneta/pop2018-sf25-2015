﻿using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.DAL
{
    public class KartaDAL
    {
        public static void UcitajKarte()
        {
            Aplikacija.Instance.Karte.Clear();

            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM KARTA";

                SqlDataAdapter daAdapter = new SqlDataAdapter();
                DataSet daLet = new DataSet();

                daAdapter.SelectCommand = command;
                daAdapter.Fill(daLet, "KARTA");

                foreach (DataRow row in daLet.Tables["KARTA"].Rows)
                {
                    Karta karta = new Karta();
                    {
                        karta.ID = (int)row["id"];
                        karta.Putnik = (string)row["putnik"];
                        karta.Sediste = (string)row["sediste"];
                        karta.UkupnaCena = (int)row["ukupnacena"];
                        karta.BiznisSediste = (bool)row["biznissediste"];
                        karta.BrojLeta = (int)row["brojleta"];
                        karta.PovratnaKarta = (bool)row["povratnakarta"];

                        Aplikacija.Instance.Karte.Add(karta);
                    }
                }
            }
        }

        public static void DodajKartu(Karta k)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO KARTA (id, putnik, sediste, ukupnacena, biznissediste, brojleta, povratnakarta) 
                                        VALUES (@id, @putnik, @sediste, @ukupnacena, @biznissediste, @brojleta, @povratnakarta)";

                command.Parameters.Add(new SqlParameter("@id", k.ID));
                command.Parameters.Add(new SqlParameter("@putnik", k.Putnik));
                command.Parameters.Add(new SqlParameter("@sediste", k.Sediste));
                command.Parameters.Add(new SqlParameter("@ukupnacena", k.UkupnaCena));
                command.Parameters.Add(new SqlParameter("@biznissediste", k.BiznisSediste));
                command.Parameters.Add(new SqlParameter("@brojleta", k.BrojLeta));
                command.Parameters.Add(new SqlParameter("@povratnakarta", k.PovratnaKarta));

                command.ExecuteNonQuery();
            }
        }
    }
}
