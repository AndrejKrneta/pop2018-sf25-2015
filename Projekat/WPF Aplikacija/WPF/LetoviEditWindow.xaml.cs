﻿using DrugiWPF.DAL;
using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for LetWindow.xaml
    /// </summary>
    public partial class LetoviEditWindow : Window
    {
        public enum Opcija { DODAJ, IZMENA }

        public DateTime _ReportPlanningDate;
        Let let;
        Opcija opcija;

        public LetoviEditWindow()
        {
            InitializeComponent();

        }
            
        public LetoviEditWindow(Let let, Opcija opcija = Opcija.DODAJ)
        {
            InitializeComponent();
            
            this.let = let;
            this.opcija = opcija;
            this.DataContext = let;

            //TxtSifra.IsEnabled = false;

            CbOdrediste.ItemsSource = Aplikacija.Instance.Aviokompanije.Select(a => a.Sifra);
            CbDestinacija.ItemsSource = Aplikacija.Instance.Aviokompanije.Select(a => a.Sifra);
            DtpVremePolaska.Value = DateTime.Now.Date;
            if (opcija.Equals(Opcija.IZMENA))
            {
                TxtSifra.IsEnabled = false;
            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = true;
            if (opcija.Equals(Opcija.DODAJ) && !PostojiLet(let.Sifra.ToString()))
            {
                //dodaj u db
                LetDAL.DodajLet(let);
                //dodaj u model
                Aplikacija.Instance.Letovi.Add(let);
                //ID FIX refresh kad doda
                LetDAL.UcitajLetove();
            }
            if (opcija.Equals(Opcija.IZMENA))
            {
                LetDAL.IzmenaLeta(let);
            }
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private bool PostojiLet(string sifra)
        {
            if (sifra !=null)
            {
                foreach (var let in Aplikacija.Instance.Letovi)
                {
                    if (let.Sifra.Equals(sifra))
                    {
                        return true;
                    }
                }
            }  
            return false;
        }


    }
}
