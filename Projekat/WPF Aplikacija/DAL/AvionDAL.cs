﻿using DrugiWPF.BLL;
using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.DAL
{
    public class AvionDAL
    {
        public static void UcitajAvione()
        {
            Aplikacija.Instance.Avioni.Clear();

            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM AVION";

                SqlDataAdapter daAdapter = new SqlDataAdapter();
                DataSet daAvion = new DataSet();

                daAdapter.SelectCommand = command;
                daAdapter.Fill(daAvion, "AVION");

                foreach (DataRow row in daAvion.Tables["AVION"].Rows)
                {
                    Avion avion = new Avion();
                    {
                        avion.ID = (int)row["id"];
                        avion.Pilot = (string)row["pilot"];
                        avion.BrojLeta = (int)row["brojleta"];
                        avion.NazivAviokompanije = (string)row["nazivaviokompanije"];
                        avion.SedisteID = (int)row["sedisteid"];
                        avion.BiznisSedisteID = (int)row["biznissedisteid"];
                        avion.BrojSedista = (int)row["brojsedista"];
                        avion.BrojBiznisSedista = (int)row["brojbiznissedista"];
                        avion.Obrisano = (bool)row["obrisano"];
                        try
                        {
                            avion.Sediste = SedisteBLL.GetById(avion.SedisteID);
                            avion.BiznisSediste = SedisteBLL.GetByIdBiznisSediste(avion.BiznisSedisteID);
                        }
                        catch (Exception e) { Console.WriteLine(e); }
                        Aplikacija.Instance.Avioni.Add(avion);
                    }
                }
            }
        }


        public static void DodajAvion(Avion a)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO AVION(pilot, brojleta, nazivaviokompanije, sedisteid, biznissedisteid, brojsedista, brojbiznissedista, obrisano) 
                                        VALUES (@pilot, @brojleta, @nazivaviokompanije, @sedisteid, @biznissedisteid, @brojsedista, @brojbiznissedista, @obrisano)";

                
                command.Parameters.Add(new SqlParameter("@pilot", a.Pilot));
                command.Parameters.Add(new SqlParameter("@brojleta", a.BrojLeta));
                command.Parameters.Add(new SqlParameter("@nazivaviokompanije", a.NazivAviokompanije));
                command.Parameters.Add(new SqlParameter("@sedisteid", a.SedisteID));
                command.Parameters.Add(new SqlParameter("@biznissedisteid", a.BiznisSedisteID));
                command.Parameters.Add(new SqlParameter("@brojsedista", a.BrojSedista));
                command.Parameters.Add(new SqlParameter("@brojbiznissedista", a.BrojBiznisSedista));
                command.Parameters.Add(new SqlParameter("@obrisano", a.Obrisano));


                command.ExecuteNonQuery();
            }
        }

        public static void IzmenaAviona(Avion a)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE AVION 
                                        SET pilot=@pilot, brojleta=@brojleta, nazivaviokompanije=@nazivaviokompanije, sedisteid=@sedisteid, biznissedisteid=@biznissedisteid, sedisteid=@sedisteid, sediste=@sediste, brojsedista=@brojsedista, brojbiznissedista=@brojbiznissedista, obrisano=@obrisano 
                                        WHERE id=@id";

                command.Parameters.Add(new SqlParameter("@id", a.ID));
                command.Parameters.Add(new SqlParameter("@pilot", a.Pilot));
                command.Parameters.Add(new SqlParameter("@brojleta", a.BrojLeta));
                command.Parameters.Add(new SqlParameter("@nazivaviokompanije", a.NazivAviokompanije));
                command.Parameters.Add(new SqlParameter("@sedisteid", a.SedisteID));
                command.Parameters.Add(new SqlParameter("@biznissedisteid", a.BiznisSedisteID));
                command.Parameters.Add(new SqlParameter("@sedisteid", a.SedisteID));
                command.Parameters.Add(new SqlParameter("@sediste", a.Sediste));
                command.Parameters.Add(new SqlParameter("@brojsedista", a.BrojSedista));
                command.Parameters.Add(new SqlParameter("@brojbiznissedista", a.BrojBiznisSedista));
                command.Parameters.Add(new SqlParameter("@obrisano", a.Obrisano));

                command.ExecuteNonQuery();
            }
        }

        public static void IzbrisiAvion(Avion a)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE AVION SET obrisano=@obrisano WHERE ID=@id";

                command.Parameters.Add(new SqlParameter("@id", a.ID));
                command.Parameters.Add(new SqlParameter("@obrisano", 1));

                command.ExecuteNonQuery();
            }
        }
    }
}
