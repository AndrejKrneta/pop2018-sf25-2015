﻿using ConsoleAppFirst.Model;
using DrugiWPF.DAL;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for AerodromiWindow.xaml
    /// </summary>
    public partial class AerodromiWindow : Window
    {
        ICollectionView view;
        public AerodromiWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Aerodromi);

            DGAerodromi.ItemsSource = view;
            DGAerodromi.IsSynchronizedWithCurrentItem = true;
            DGAerodromi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            view.Filter = CustomFilter;
            view.Refresh();
        }

        
        private bool CustomFilter(object obj)
        {
            Aerodrom aerodrom = obj as Aerodrom;
            if (TxtSearch.Text.Equals(String.Empty))
            {
                return !aerodrom.Obrisano;
            }
            else
            {
                //pretvara aeordorm na lover case i text na lover da bi mogli traziti bez greski
                return !aerodrom.Obrisano ; 
            }
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BTNDodaj_Click(object sender, RoutedEventArgs e)
        {
            EditAerodromWindow aew = new EditAerodromWindow(new Aerodrom(), EditAerodromWindow.Opcija.DODAVANJE);
            if (aew.ShowDialog() == true)
            {
                view.Refresh();
            }
        }

        private void BTNIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom)DGAerodromi.SelectedItem;
            if (SelektovanAerodrom(aerodrom))
            {
                Aerodrom stari = aerodrom.Clone() as Aerodrom;
                EditAerodromWindow aew = new EditAerodromWindow(aerodrom, EditAerodromWindow.Opcija.IZMENA);
                if (aew.ShowDialog() != true)
                {
                    int indeks = IndeksSelektovanogAerodroma(aerodrom.Sifra);
                    Aplikacija.Instance.Aerodromi[indeks] = stari;
                }
            }
        }

        private void BTNObrisi_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom)DGAerodromi.SelectedItem;
            if (SelektovanAerodrom(aerodrom))
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksSelektovanogAerodroma(aerodrom.Sifra);
                    //aerodrom.Izmena();
                    Aplikacija.Instance.Aerodromi[indeks].Obrisano = true;
                    //DB Logicko birsanje
                    AerodromDAL.IzbrisiAerodrom(aerodrom);
                    view.Refresh();
                }
            }
        }

        private int IndeksSelektovanogAerodroma(String sifra)
        {
            var indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Aerodromi.Count; i++)
            {
                if (Aplikacija.Instance.Aerodromi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;

                }
            }
            return indeks;
        }

        private bool SelektovanAerodrom(Aerodrom aerodrom)
        {
            if (aerodrom == null)
            {
                MessageBox.Show("Nije selektovan aerodrom");
                return false;
            }
            return true;
        }
        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
