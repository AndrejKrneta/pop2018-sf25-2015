﻿using ConsoleAppFirst.Model;
using DrugiWPF.DAL;
using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for AvionWindow.xaml
    /// </summary>
    public partial class AvionWindow : Window
    {
        public enum PretragaPO { BROJLETA, AVIOKOMPANIJA}
        ICollectionView view;

        public AvionWindow()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Avioni);
            DGAvioni.ItemsSource = view;
            DGAvioni.IsSynchronizedWithCurrentItem = true;
            DGAvioni.DataContext = this;
            DGAvioni.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            //Vizualno brisanje sa liste kad se obrise
            view.Filter = CustomFilter;

            NapuniComboBox();
        }

        private void DGAvioni_AutoGeneratingColumn(object sender, System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString() == "Sediste" || e.Column.Header.ToString() == "SedisteID"
                || e.Column.Header.ToString() == "BiznisSediste" || e.Column.Header.ToString() == "BiznisSedisteID")
            {
                e.Cancel = true;
            }
        }

        private bool CustomFilter(object obj)
        {
            Avion avion = obj as Avion;
            switch (CBPretragaPo.SelectedValue)
            {

                case PretragaPO.BROJLETA:
                    return !avion.Obrisano && avion.BrojLeta.ToString().ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.AVIOKOMPANIJA:
                    return !avion.Obrisano && avion.NazivAviokompanije.ToString().ToLower().Contains(TBPretraga.Text.ToLower());

                default:
                    return !avion.Obrisano;
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AvionEditWindow kew = new AvionEditWindow(new Avion(), AvionEditWindow.Opcija.DODAJ);
            if (kew.ShowDialog() == true)
            {

                view.Refresh();
            }
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = (Avion)DGAvioni.SelectedItem;
            if (SelektovanAvion(avion))
            {
                Avion stari = avion.Clone() as Avion;
                AvionEditWindow aew = new AvionEditWindow(avion, AvionEditWindow.Opcija.IZMENA);
                if (aew.ShowDialog() != true)
                {
                    int indeks = IndeksSelektovanogAviona(avion.BrojLeta);
                    Aplikacija.Instance.Avioni[indeks] = stari;
                    view.Refresh();
                }
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {

            Avion avion = (Avion)DGAvioni.SelectedItem;
            if (SelektovanAvion(avion))
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksSelektovanogAviona(avion.BrojLeta);
                    if (indeks != -1)
                    {
                        //model izbrisi
                        Aplikacija.Instance.Avioni[indeks].Obrisano = true;
                        //db izbrisi
                        AvionDAL.IzbrisiAvion(avion);
                    }
                    view.Refresh();
                }
            }
        }
        private int IndeksSelektovanogAviona(int brojLeta)
        {
            var indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Avioni.Count; i++)
            {
                if (Aplikacija.Instance.Avioni[i].BrojLeta.Equals(brojLeta))
                {
                    indeks = i;
                    break;

                }
            }
            return indeks;
        }
        private bool SelektovanAvion(Avion avion)
        {
            if (avion == null)
            {
                MessageBox.Show("Nije selektovan avion");
                return false;
            }
            return true;
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void NapuniComboBox()
        {
            CBPretragaPo.ItemsSource = Enum.GetValues(typeof(PretragaPO));
        }

        private void TBPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
