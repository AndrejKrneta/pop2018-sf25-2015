﻿using ConsoleAppFirst.Model;
using DrugiWPF.BLL;
using DrugiWPF.DAL;
using DrugiWPF.DAL.SQL;
using DrugiWPF.Models;
using DrugiWPF.Util;
using DrugiWPF.WPF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrugiWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ICollectionView view;

        public Korisnik _korisnik;
        private ETipKorisnika _tipKorisnika = ETipKorisnika.NEREGISTROVAN;

        public Let _let;
        public MainWindow()
        {
            UcitajDB();
            InitializeComponent();
            CBVasaLokacija.ItemsSource = Aplikacija.Instance.Aviokompanije.Select(a => a.Sifra);
            CBDestinacija.ItemsSource = Aplikacija.Instance.Aviokompanije.Select(a => a.Sifra);
            CBPol.ItemsSource = Enum.GetValues(typeof(EPol));
        }

        private void UcitajDB()
        {
            KorisnikDAL.UcitajKorisnike();
            SedisteDAL.UcitajSedista();
            LetDAL.UcitajLetove(); 
            AvionDAL.UcitajAvione();
            AviokompanijaDAL.UcitajAviokompanije();
            KartaDAL.UcitajKarte();
            AerodromDAL.UcitajAerodrome();
        }

        private void BtnBiranjeSedista_Click(object sender, RoutedEventArgs e)
        {

            BtnSelected(BtnBiranjeSedista);
            _let = DGLetovi.SelectedItem as Let;
            if (DGLetovi.SelectedIndex >= 0 && !(AvionBLL.GetByBrojLeta(_let.Sifra) == null))
            {
                KupovinaKarteWindow kkw = new KupovinaKarteWindow(_let, AvionBLL.GetByBrojLeta(_let.Sifra),_korisnik);
                kkw.Show();
            }
            else
            {
                MessageBox.Show("Molimo vas da selektujete let ili trenutno nemamo dostupan avion");
            }
        }



        private void BTNRegister_Click(object sender, RoutedEventArgs e)
        {

            ShowCan(CRegister);
            HiddeCan(CLogin);
        }

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {

            BtnSelected(BtnAerodromi);

            AerodromiWindow aw = new AerodromiWindow();
            aw.Show();

        }

        private void BtnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            BtnSelected(BtnKorisnici);

            KorisniciWindow kw = new KorisniciWindow();
            kw.Show();
        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            BtnSelected(BtnLetovi);

            LetoviWindow lw = new LetoviWindow();
            lw.Show();
        }

        private void BTNSedista_Click(object sender, RoutedEventArgs e)
        {
            BtnSelected(BTNSedista);
            SedistaWindow sw = new SedistaWindow();
            sw.Show();
        }

        private void BTNAviokompanija_Click(object sender, RoutedEventArgs e)
        {
            BtnSelected(BTNAviokompanija);
            AviokompanijaWindow aw = new AviokompanijaWindow();
            aw.Show();
        }

        private void BTNAvion_Click(object sender, RoutedEventArgs e)
        {
            BtnSelected(BTNAvion);
            AvionWindow aww = new AvionWindow();
            aww.Show();
        }

        private void BTNSviLetovi_Click(object sender, RoutedEventArgs e)
        {
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Letovi);

            DGLetovi.ItemsSource = view;
            DGLetovi.IsSynchronizedWithCurrentItem = true;
            DGLetovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            view.Filter = CustomFilterEveryThing;
        }

        private void BTNKarte_Click(object sender, RoutedEventArgs e)
        {
            KarteWindow kw = new KarteWindow(_korisnik.ID, _korisnik);
            kw.Show();
        }

        private void BTNNazad_Click(object sender, RoutedEventArgs e)
        {
            ShowCan(CLogin);
            HiddeCan(CRegister);
        }

        private void BTNLogin_Click(object sender, RoutedEventArgs e)
        {
            var username = TBUserName.Text;
            var password = PBPassword.Password;
            KorisnikDAL.UcitajKorisnike();
            foreach (var korisnik in Aplikacija.Instance.Korisnici)
            {
                
                if ((korisnik.KorisnickoIme == username && korisnik.Lozinka == password) && !korisnik.Obrisano)
                {

                    //postavljanje trenutnog loginovanog korisnika i njegov Etipk
                    this._korisnik = korisnik;
                    this._tipKorisnika = korisnik.TipKorisnika;

                    //Postavljanje imena i tipa korisnika 
                    LUserName.Content = "User Name: " + _korisnik.KorisnickoIme;
                    LTipKorisnika.Content = "Tip Korisnika: " + _korisnik.TipKorisnika;

                    //login canvas sakriti i btn logout stavlja se visible
                    HiddeCan(CLogin);
                    ShowCan(CLetovi);
                    ShowBtn(BTNLogout);

                    //ShowBtn(BTNIzmenaKorisnika);

                    //prevjera tipa korisnika koje opcije trba da ima
                    if (ETipKorisnika.ADMINISTRATOR == _tipKorisnika)
                    {
                        ShowCan(CanSelectedBtn);

                        ShowBtn(BtnAerodromi);
                        ShowBtn(BtnKorisnici);
                        ShowBtn(BTNAviokompanija);
                        ShowBtn(BTNAvion);
                        ShowBtn(BTNSedista);
                        ShowBtn(BtnLetovi);
                    }

                }
                else
                {
                    LBInfo.Content = "Netacni podaci";
                }
            }
        }

        private void BTNLogout_Click(object sender, RoutedEventArgs e)
        {
            if (_korisnik != null)
            {
                _korisnik = null;
                _tipKorisnika = ETipKorisnika.NEREGISTROVAN;
                ShowCan(CLogin);
                HiddeCan(CLetovi);
                HiddeCan(CanSelectedBtn);

                HiddeBtn(BTNLogout);
                HiddeBtn(BtnAerodromi);
                HiddeBtn(BtnKorisnici);
                HiddeBtn(BTNAvion);
                HiddeBtn(BtnLetovi);
                HiddeBtn(BTNAviokompanija);
                HiddeBtn(BTNSedista);

                HiddeBtn(BTNIzmenaKorisnika);

                LUserName.Content = "User Name: Guest";
                LTipKorisnika.Content = "Tip Korisnika: NEREGISTROVAN";

                BtnSelected(BtnLetovi);
            }
        }


        private void BTNRegistrirajSe_Click(object sender, RoutedEventArgs e)
        {
            

            if (!String.IsNullOrEmpty(TBKorisnikoIme.Text) && !ProvjeraPostojacegKorisnickogImena(TBKorisnikoIme.Text) && !String.IsNullOrEmpty(TBEmail.Text))
            {
                try
                {
                    var emailAdresa = new MailAddress(TBEmail.Text);

                    Korisnik noviK = new Korisnik()
                    {
                        Ime = TBIme.Text,
                        Prezime = TBPrezime.Text,
                        KorisnickoIme = TBKorisnikoIme.Text,
                        Lozinka = TBLozinka.Text,
                        AdresaStanovanja = TBAdresaStanovanja.Text,
                        Email = TBEmail.Text,
                        Pol = (EPol)Enum.Parse(typeof(EPol), CBPol.Text),
                        TipKorisnika = ETipKorisnika.PUTNIK
                    };

                    //dodaj novog korisnika
                    KorisnikDAL.DodajKorisnika(noviK);
                    //refresh korisnike
                    KorisnikDAL.UcitajKorisnike();
                    LBInfo.Content = "Uspjesno ste se registrovali";

                    ShowCan(CLogin);
                    HiddeCan(CRegister);
                }
                catch (FormatException ex)
                {
                    MessageBox.Show("Pogresan format emaila");
                }

            }
            else
            {
                MessageBox.Show("Molimo vas da ispunite sva polja");
            }

        }
        
        /*
        private Karta ProvjeraPostojaceKarte(int id)
        {
            foreach (var k in Aplikacija.Instance.Karte)
            {
                if (k.ID.Equals(id))
                {
                    return k;
                }
            }
            return null;
        }
        */

        private bool ProvjeraPostojacegKorisnickogImena(string kIme)
        {
            foreach (var korisnik in Aplikacija.Instance.Korisnici)
            {
                if(korisnik.KorisnickoIme == kIme)
                {
                    return true;
                }
            }
            return false;
        }


        private void BTNFiltriraj_Click(object sender, RoutedEventArgs e)
        {
            if(CBDestinacija.SelectedValue == CBVasaLokacija.SelectedValue)
            {
                MessageBox.Show("Vasa lokacija i destinacije ne mogu biti isti");
            }
            else if(CBDestinacija.SelectedIndex == -1 || CBVasaLokacija.SelectedIndex == -1)
            {

                MessageBox.Show("Molimo vas da selektujete vasu lokaciju i destinaciju");
            }
            else
            {
                view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Letovi);

                DGLetovi.ItemsSource = view;
                DGLetovi.IsSynchronizedWithCurrentItem = true;
                DGLetovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

                view.Filter = CustomFilter;
            }

        }

        private bool CustomFilter(object obj)
        {
            Let let = obj as Let;
            if (CBDestinacija.SelectedValue.ToString() == let.Destinacija.ToString() && CBVasaLokacija.SelectedValue.ToString() == let.Odrediste.ToString())
            {
                
                return !let.Obrisano && CBDestinacija.SelectedValue.ToString() == let.Destinacija.ToString()& CBVasaLokacija.SelectedValue.ToString() == let.Odrediste.ToString();
            }
            else if(CBDestinacija.Equals(string.Empty))
            {

                return !let.Obrisano;
            }
            else
            {
                return false;
            }
        }

        private bool CustomFilterEveryThing(object obj)
        {
            Let let = obj as Let;
            return !let.Obrisano;
        }

        private void HiddeBtn(Button btn)
        {
            btn.Visibility = Visibility.Hidden;
        }

        private void HiddeCan(Canvas can)
        {
            can.Visibility = Visibility.Hidden;
        }

        private void ShowCan(Canvas can)
        {
            can.Visibility = Visibility.Visible;
        }

        private void ShowBtn(Button btn)
        {
            btn.Visibility = Visibility.Visible;
        }

        private void BtnSelected(Button btn)
        {
            Thickness margin = CanSelectedBtn.Margin;
            margin.Top = btn.Margin.Top;
            CanSelectedBtn.Margin = margin;
            CanSelectedBtn.Height = btn.Height;
        }

        private void BTNIzmenaKorisnika_Click(object sender, RoutedEventArgs e)
        {
            KorisniciEditWindow ekw = new KorisniciEditWindow(_korisnik);
            if (ekw.ShowDialog() != true)
            {
                view.Refresh();
            }
        }

        private void DGLetovi_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (!_korisnik.TipKorisnika.Equals(ETipKorisnika.ADMINISTRATOR))
            {
                if (e.Column.Header.ToString() == "ID" || e.Column.Header.ToString() == "Obrisano")
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
