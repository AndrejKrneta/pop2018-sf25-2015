﻿using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for SedistaWindow.xaml
    /// </summary>
    public partial class SedistaWindow : Window
    {
        ICollectionView view;
        public SedistaWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Sedista);
            DGSedista.ItemsSource = view;
            DGSedista.IsSynchronizedWithCurrentItem = true;
            DGSedista.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }
    }
}
