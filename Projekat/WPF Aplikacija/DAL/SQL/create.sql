﻿SELECT * FROM LET

CREATE TABLE [dbo].[LET] (
    [id]           INT           IDENTITY (1, 1) NOT NULL,
    [sifra]        INT			 NOT NULL,
    [odrediste]    VARCHAR (255) NOT NULL,
    [destinacija]  VARCHAR (255) NOT NULL,
    [vremepolaska] DATE          NOT NULL,
    [vremedolaska] DATE          NOT NULL,
    [cenakarte]    INT           NOT NULL,
    [Obrisano]     BIT           NOT NULL,
    CONSTRAINT [PK_LET] PRIMARY KEY CLUSTERED ([id] ASC)
);

CREATE TABLE [dbo].[KORISNIK] (
    [id]            INT          IDENTITY (1, 1) NOT NULL,
    [ime]           VARCHAR (15) NOT NULL,
    [prezime]       VARCHAR (15) NOT NULL,
    [korisnickoime] VARCHAR (30) NOT NULL,
    [lozinka]       VARCHAR (30) NOT NULL,
    [obrisano]      BIT          NOT NULL,
    [tipkorisnika]  VARCHAR (15) NOT NULL,
    CONSTRAINT [PK_KORISNIK] PRIMARY KEY CLUSTERED ([id] ASC)
);

CREATE TABLE [dbo].[AERODROM] (
    [id]       INT           IDENTITY (1, 1) NOT NULL,
    [sifra]    VARCHAR (15)  NOT NULL,
    [naziv]    VARCHAR (255) NOT NULL,
    [grad]     VARCHAR (255) NOT NULL,
    [obrisano] BIT           NOT NULL,
    CONSTRAINT [PK_AERODROM] PRIMARY KEY CLUSTERED ([id] ASC)
);

CREATE TABLE [dbo].[AVION] (
    [id]                 INT           IDENTITY (1, 1) NOT NULL,
    [pilot]              VARCHAR (15)  NOT NULL,
    [brojleta]           INT           NOT NULL,
    [nazivaviokompanije] VARCHAR (255) NOT NULL,
    [sedisteid]          INT           NOT NULL,
    [biznissedisteid]    INT           NOT NULL,
    [brojsedista]		 INT           NOT NULL,
    [brojbiznissedista]  INT           NOT NULL,
    [obrisano]           BIT           NOT NULL,
    CONSTRAINT [PK_AVION] PRIMARY KEY CLUSTERED ([id] ASC)
);

CREATE TABLE [dbo].[SEDISTE] (
    [id]				INT				NOT NULL,
    [slobodnasedista]   VARCHAR (255)	NOT NULL,
    [zauzetasedista]    VARCHAR (255)	NOT NULL,
    [biznissediste]		BIT				NOT NULL
);

CREATE TABLE [dbo].[AVIOKOMPANIJA] (
    [id]                 INT           IDENTITY (1, 1) NOT NULL,
    [sifra]				 VARCHAR (255) NOT NULL,
    [letid]				 INT           NOT NULL,
    [obrisano]           BIT           NOT NULL,
    CONSTRAINT [PK_AVIOKOMPANIJA] PRIMARY KEY CLUSTERED ([id] ASC)
);

CREATE TABLE [dbo].[KARTA] (
    [id]                 INT           NOT NULL,
    [brojleta]			 INT		   NOT NULL,
    [sediste]			 VARCHAR (255) NOT NULL,
    [putnik]			 VARCHAR (255) NOT NULL,
    [ukupnacena]		 INT		   NOT NULL,
    [biznissediste]      BIT           NOT NULL,
    [povratnakarta]		 BIT		   NOT NULL
);

drop table KARTA

SELECT * FROM SEDISTE

SELECT * FROM AVION

SELECT * FROM AVIOKOMPANIJA

drop table AVIOKOMPANIJA