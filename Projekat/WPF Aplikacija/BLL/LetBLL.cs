﻿
using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.BLL
{
    class LetBLL
    {
        public static Let GetById(int id)
        {
            foreach (var let in Aplikacija.Instance.Letovi)
            {
                if (let.ID == id)
                {
                    return let;
                }
            }
            return null;
        }
    }
}
