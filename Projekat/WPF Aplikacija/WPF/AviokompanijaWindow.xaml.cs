﻿using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for AviokompanijaWindow.xaml
    /// </summary>
    public partial class AviokompanijaWindow : Window
    {
        ICollectionView view;
        public AviokompanijaWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Aviokompanije);
            DGAviokompanija.ItemsSource = view;
            DGAviokompanija.IsSynchronizedWithCurrentItem = true;
            DGAviokompanija.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            //AviokompanijaEditWindow aew = new AviokompanijaEditWindow();
            //aew.Show();
        }
    }
}
