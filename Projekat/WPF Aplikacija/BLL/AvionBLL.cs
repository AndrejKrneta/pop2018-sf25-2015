﻿using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.BLL
{
    class AvionBLL
    {
        public static Avion GetById(int id)
        {
            foreach (var avion in Aplikacija.Instance.Avioni)
            {
                if (avion.ID == id)
                {
                    return avion;
                }
            }
            return null;
        }

        public static Avion GetByBrojLeta(int brojLeta)
        {
            foreach (var avion in Aplikacija.Instance.Avioni)
            {
                if (avion.BrojLeta == brojLeta)
                {
                    return avion;
                }
            }
            return null;
        }
    }
}
