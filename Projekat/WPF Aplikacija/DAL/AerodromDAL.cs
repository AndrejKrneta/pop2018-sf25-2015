﻿using ConsoleAppFirst.Model;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.DAL
{
    public class AerodromDAL
    {
        public static void UcitajAerodrome()
        {
            Aplikacija.Instance.Aerodromi.Clear();

            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM AERODROM";

                SqlDataAdapter daAdapter = new SqlDataAdapter();
                DataSet daA = new DataSet();

                daAdapter.SelectCommand = command;
                daAdapter.Fill(daA, "AERODROM");

                foreach (DataRow row in daA.Tables["AERODROM"].Rows)
                {
                    Aerodrom a = new Aerodrom();
                    {
                        a.ID = (int)row["id"];
                        a.Sifra = (string)row["sifra"];
                        a.Naziv = (string)row["naziv"];
                        a.Grad = (string)row["grad"];
                        a.Obrisano = (bool)row["obrisano"];

                        Aplikacija.Instance.Aerodromi.Add(a);
                    }
                }
            }
        }

        public static void DodajAerodrom(Aerodrom a)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO AERODROM(sifra, naziv, grad, obrisano) 
                                        VALUES (@sifra, @naziv, @grad, @obrisano)";


                command.Parameters.Add(new SqlParameter("@sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@naziv", a.Naziv));
                command.Parameters.Add(new SqlParameter("@grad", a.Grad));
                command.Parameters.Add(new SqlParameter("@obrisano", a.Obrisano));

                command.ExecuteNonQuery();
            }
        }

        public static void IzmenaAerodroma(Aerodrom a)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE AERODROM 
                                        SET sifra=@sifra, naziv=@naziv, grad=@grad, obrisano=@obrisano 
                                        WHERE id=@id";

                command.Parameters.Add(new SqlParameter("@id", a.ID));
                command.Parameters.Add(new SqlParameter("@sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@naziv", a.Naziv));
                command.Parameters.Add(new SqlParameter("@grad", a.Grad));
                command.Parameters.Add(new SqlParameter("@obrisano", a.Obrisano));

                command.ExecuteNonQuery();
            }
        }
        public static void IzbrisiAerodrom(Aerodrom a)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE AERODROM SET obrisano=@obrisano WHERE ID=@id";

                command.Parameters.Add(new SqlParameter("@id", a.ID));
                command.Parameters.Add(new SqlParameter("@obrisano", 1));

                command.ExecuteNonQuery();
            }
        }
    }
}
