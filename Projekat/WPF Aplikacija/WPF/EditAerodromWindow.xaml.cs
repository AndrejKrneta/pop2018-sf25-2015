﻿using ConsoleAppFirst.Model;
using DrugiWPF.DAL;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for EditAerodromWindow.xaml
    /// </summary>
    public partial class EditAerodromWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA }

        Aerodrom aerodrom;
        Opcija opcija;

        public EditAerodromWindow(Aerodrom aerodrom, Opcija opcija)
        {
            InitializeComponent();

            this.aerodrom = aerodrom;
            this.opcija = opcija;
            this.DataContext = aerodrom;

            if (opcija.Equals(Opcija.IZMENA))
            {
                TXTSifra.IsEnabled = false;
            }
        }

        private void BTNSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            
            this.DialogResult = true;
            if (opcija.Equals(Opcija.DODAVANJE) && !PostojiAerodrom(aerodrom.Sifra))
            {
                //dodaj u db
                AerodromDAL.DodajAerodrom(aerodrom);
                //dodaj u model
                Aplikacija.Instance.Aerodromi.Add(aerodrom);
                //ID FIX refresh kad doda
                AerodromDAL.UcitajAerodrome();
            }
            if (opcija.Equals(Opcija.IZMENA))
            {
                AerodromDAL.IzmenaAerodroma(aerodrom);
            }

        }

        private void BTNObrisi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private bool PostojiAerodrom(string sifra)
        {
            foreach (var aerodrom in Aplikacija.Instance.Aerodromi)
            {
                if (aerodrom.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
