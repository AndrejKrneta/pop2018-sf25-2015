﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppFirst.Model
{
    public class Korisnik : INotifyPropertyChanged, ICloneable
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private String ime;
        public String Ime
        {
            get { return ime; }
            set { ime = value; OnPropertyChanged("Ime"); }
        }

        private String prezime;
        public String Prezime
        {
            get { return prezime; }
            set { prezime = value; OnPropertyChanged("Prezime"); }
        }

        private String korisnickoIme;

        public String KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; OnPropertyChanged("KorisnickoIme"); }
        }

        private String lozinka;

        public String Lozinka
        {
            get { return lozinka; }
            set { lozinka = value; OnPropertyChanged("Lozinka"); }
        }

        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged("Email"); }
        }

        private String adresaStanovanaj;

        public String AdresaStanovanja
        {
            get { return adresaStanovanaj; }
            set { adresaStanovanaj = value; }
        }

        private ETipKorisnika tipKorisnika;

        public ETipKorisnika TipKorisnika
        {
            get { return tipKorisnika; }
            set { tipKorisnika = value; OnPropertyChanged("TipKorisnika"); }
        }

        private EPol pol;

        public EPol Pol
        {
            get { return pol; }
            set { pol = value; OnPropertyChanged("Pol"); }
        }

        private bool obrisano;

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return $"Ime {Ime}, Prezime {Prezime}, Tip Korisnika {TipKorisnika}, Pol {Pol}";
        }


        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        public Korisnik()
        {

        }
        //privremeno za sad
        public Korisnik(int id,String ime, String prezime, String korisnikoIme, String lozinka, ETipKorisnika tipKorisnika)
        {
            this.id = id;
            this.Ime = ime;
            this.Prezime = prezime;
            this.KorisnickoIme = korisnikoIme;
            this.Lozinka = lozinka;
            this.TipKorisnika = tipKorisnika;
        }

        //kako bi trbalo se praviti
        public Korisnik(String ime, String prezime, String korisnikoIme, String lozinka, String email, String adresaStanovanja, ETipKorisnika tipKorisnika, EPol pol)
        {
            this.Ime = ime;
            this.Prezime = prezime;
            this.KorisnickoIme = korisnikoIme;
            this.Lozinka = lozinka;
            this.Email = email;
            this.AdresaStanovanja = adresaStanovanja;
            this.TipKorisnika = TipKorisnika;
            this.Pol = pol;
        }

        public object Clone()
        {
            Korisnik novi = new Korisnik
            {
                ID = this.id,
                Ime = this.ime,
                KorisnickoIme = this.korisnickoIme,
                Prezime = this.prezime,
                Lozinka = this.Lozinka,
                Email = this.email,
                AdresaStanovanja = this.adresaStanovanaj,
                TipKorisnika = this.tipKorisnika,
                Pol = this.pol
            };
            return novi;
        }

    }
}
