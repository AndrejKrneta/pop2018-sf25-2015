﻿using ConsoleAppFirst.Model;
using DrugiWPF.Util;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DrugiWPF.DAL
{
    public class KorisnikDAL
    {
        public static void UcitajKorisnike()
        {
            Aplikacija.Instance.Korisnici.Clear();

            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM KORISNIK";

                SqlDataAdapter daAdapter = new SqlDataAdapter();
                DataSet daKorisnici = new DataSet();

                daAdapter.SelectCommand = command;
                daAdapter.Fill(daKorisnici, "KORISNIK");

                foreach (DataRow row in daKorisnici.Tables["KORISNIK"].Rows)
                {
                    Korisnik korisnik = new Korisnik();
                    {
                        korisnik.ID = (int)row["id"];
                        korisnik.Ime = (string)row["ime"];
                        korisnik.Prezime = (string)row["prezime"];
                        korisnik.KorisnickoIme = (string)row["korisnickoime"];
                        korisnik.Lozinka = (string)row["lozinka"];
                        korisnik.Obrisano = (bool)row["obrisano"];
                        korisnik.TipKorisnika = (ETipKorisnika)Enum.Parse(typeof(ETipKorisnika), (string)row["tipkorisnika"]);

                        Aplikacija.Instance.Korisnici.Add(korisnik);
                    }
                }
            }
        }

        public static void DodajKorisnika(Korisnik k)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO KORISNIK ( ime, prezime, korisnickoime, lozinka, obrisano, tipkorisnika) 
                                        VALUES (@ime, @prezime, @korisnickoime, @lozinka, @obrisano, @tipkorisnika)";


                command.Parameters.Add(new SqlParameter("@ime", k.Ime));
                command.Parameters.Add(new SqlParameter("@prezime", k.Prezime));
                command.Parameters.Add(new SqlParameter("@korisnickoime", k.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@lozinka", k.Lozinka));
                command.Parameters.Add(new SqlParameter("@obrisano", k.Obrisano));
                command.Parameters.Add(new SqlParameter("@tipkorisnika", k.TipKorisnika.ToString()));

                command.ExecuteNonQuery();
            }
        }
        public static void IzmenaKorisnika(Korisnik k)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE KORISNIK 
                                        SET ime=@ime, prezime=@prezime, korisnickoime=@korisnickoime, lozinka=@lozinka, 
                                            obrisano=@obrisano, tipkorisnika=@tipkorisnika WHERE id=@id";

                command.Parameters.Add(new SqlParameter("@id", k.ID));
                command.Parameters.Add(new SqlParameter("@ime", k.Ime));
                command.Parameters.Add(new SqlParameter("@prezime", k.Prezime));
                command.Parameters.Add(new SqlParameter("@korisnickoime", k.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@lozinka", k.Lozinka));
                command.Parameters.Add(new SqlParameter("@obrisano", k.Obrisano));
                command.Parameters.Add(new SqlParameter("@tipkorisnika", k.TipKorisnika.ToString()));

                command.ExecuteNonQuery();
            }
        }

        public static void IzbrisiKorisnika(Korisnik k)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE KORISNIK SET obrisano=@obrisano WHERE ID=@id";

                command.Parameters.Add(new SqlParameter("@id", k.ID));
                command.Parameters.Add(new SqlParameter("@obrisano", 1));

                command.ExecuteNonQuery();
            }
        }


    }
}
