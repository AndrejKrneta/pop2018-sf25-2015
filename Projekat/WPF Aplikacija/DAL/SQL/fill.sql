﻿SELECT * FROM LET

INSERT INTO AERODRM(sifra, naziv, grad, obrisano) 
	VALUES ('BEG', 'Nikola Tesla', 'Beograd', 0)

INSERT INTO AERODROM(sifra, naziv, grad, obrisano) 
VALUES ('AMS', 'Amsterdam Aerodrom', 'Amsterdam', 0)

INSERT INTO LET(sifra, odrediste, destinacija, vremepolaska, vremedolaska, cenakarte, obrisano)
	VALUES(112, 'BEG', 'AMS', '2018-11-20', '2018-11-21', 4000, 0)

INSERT INTO LET(sifra, odrediste, destinacija, vremepolaska, vremedolaska, cenakarte, obrisano)
	VALUES(212, 'LON', 'AMS', '2018-12-21', '2018-12-22', 2000, 0)

INSERT INTO AVION(pilot, brojleta, nazivaviokompanije, sedisteid, biznissedisteid, brojsedista, brojbiznissedista, obrisano)
VALUES('Marko markovic', '21', 'Airlines NS', 1, 2, 36, 6, 0)

INSERT INTO KARTA(id, brojleta, sediste, putnik, ukupnacena, biznissediste)
VALUES(1,1, 'A1', 'Andrej', 15000, 1)

INSERT INTO KARTA(id, brojleta, sediste, putnik, ukupnacena, biznissediste)
VALUES(1,3, 'A2', 'Andrej', 12000, 1)

INSERT INTO SEDISTE(slobodnasedista, zauzetasedista, obrisano)
VALUES('A1', 'B1', 0)

INSERT INTO SEDISTE(slobodnasedista, zauzetasedista, obrisano)
VALUES('A2', 'B2', 0)

INSERT INTO AVIOKOMPANIJA(sifra, letid, obrisano)
VALUES('BEG', 1, 0)

INSERT INTO AVIOKOMPANIJA(sifra, letid, obrisano)
VALUES('LON', 2, 0)

INSERT INTO AVIOKOMPANIJA(sifra, letid, obrisano)
VALUES('AMS', 3, 0)

SELECT * FROM AVIOKOMPANIJA

SELECT * FROM LET

SELECT * FROM SEDISTE

SELECT * FROM AVION
