﻿using ConsoleAppFirst.Model;
using DrugiWPF.BLL;
using DrugiWPF.DAL;
using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for KupovinaKarteWindow.xaml
    /// </summary>
    public partial class KupovinaKarteWindow : Window
    {
        public enum KlasaSedista { BIZNIS,EKONOMSKA}

        public Avion avion;
        public Let let;
        public Korisnik korisnik;
        public bool biznisSediste;
        public Sediste sediste;
        public KupovinaKarteWindow(Let _let, Avion _avion,Korisnik _korisnik)
        {
            let = _let;
            avion = _avion;
            korisnik = _korisnik;
            InitializeComponent();
            NapuniComboBox();
        }

        private void NapuniComboBox()
        {
            CBKlasaSedista.ItemsSource = Enum.GetValues(typeof(KlasaSedista));
        }

        private void BTNIzlistajSedista_Click(object sender, RoutedEventArgs e)
        {
            //clear cb da se ne stakuje
            CBSjediste.Items.Clear();

            switch (CBKlasaSedista.SelectedValue)
            {
                case KlasaSedista.BIZNIS:
                    biznisSediste = true;
                    Biznis(avion);
                    break;
                case KlasaSedista.EKONOMSKA:
                    biznisSediste = false;
                    Ekonomska(avion);
                    break;
            }
        }

        private void Biznis(Avion avion)
        {
            foreach (var item in SedisteBLL.ListaSvihBiznisSedista(avion))
            {
                CBSjediste.Items.Add(item);
            }
        }

        private void Ekonomska(Avion avion)
        {
            foreach (var item in SedisteBLL.ListaSvihSedista(avion))
            {
                CBSjediste.Items.Add(item);
            }
        }

        private void BTNKupiKartu_Click(object sender, RoutedEventArgs e)
        {

            if (CBSjediste.SelectedItem != null)
            {
                Karta k = new Karta()
                {
                    BiznisSediste = biznisSediste,
                    BrojLeta = let.Sifra,
                    ID = korisnik.ID,
                    Putnik = korisnik.Ime,
                    Sediste = CBSjediste.SelectedValue.ToString(),
                    PovratnaKarta = false,
                    //ako je biznis klasa * 1.5 cena ako je ekonomska onda UkupnaCena = CenaKarte 
                    UkupnaCena = Convert.ToInt32(biznisSediste ? let.CenaKarte * 1.5 : let.CenaKarte)
                        
                };
                KartaDAL.DodajKartu(k);
                Aplikacija.Instance.Karte.Add(k);
                if ((bool)CBPovratnaKarta.IsChecked)
                {
                    k.PovratnaKarta = true;
                    KartaDAL.DodajKartu(k);
                    Aplikacija.Instance.Karte.Add(k);
                }
                //Ako je biznis sediste true onda prosledi biznis sedista ako je false prosledi sedista(ekonomska)

                if (biznisSediste)
                    sediste = avion.BiznisSediste;
                if(!biznisSediste)
                    sediste = avion.Sediste;

                //db update
                SedisteDAL.UpadteSedista(SedisteBLL.SlobodnoNaZauzetoSediste(CBSjediste.SelectedValue.ToString(), biznisSediste ? avion.BiznisSediste : avion.Sediste));
                

                this.Close();
            }
            else
            {
                MessageBox.Show("Izaberite Sediste");
            }
            
        }
    }
}
