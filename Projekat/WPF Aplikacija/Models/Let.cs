﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.Models
{
    public class Let : INotifyPropertyChanged, ICloneable
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; OnPropertyChanged("ID"); }
        }

        private string destinacija;

        public string Destinacija
        {
            get { return destinacija; }
            set { destinacija = value; OnPropertyChanged("Destinacija"); }
        }
        private int sifra;

        public int Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private DateTime vremePolaska = DateTime.Now;

        public DateTime VremePolaska
        {
            get { return vremePolaska; }
            set { vremePolaska = value; OnPropertyChanged("VremePolaska"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private DateTime vremeDolaska = DateTime.Now;
        
        public DateTime VremeDolaska
        {
            get { return vremeDolaska; }
            set { vremeDolaska = value; OnPropertyChanged("VremeDolaska"); }
        }

        private string odrediste;

        public string Odrediste
        {
            get { return odrediste; }
            set { odrediste = value; }
        }

        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private int cenaKarte;

        public int CenaKarte
        {
            get { return cenaKarte; }
            set { cenaKarte = value; OnPropertyChanged("VremeDolaska");}
        }

        private bool obrisano;

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }

        public object Clone()
        {
            Let novi = new Let
            {
                ID = this.id,
                Sifra = this.sifra,
                Odrediste = this.odrediste,
                Destinacija = this.destinacija,
                VremePolaska = this.vremePolaska,
                VremeDolaska = this.vremeDolaska,
                CenaKarte = this.cenaKarte,
                Obrisano = this.obrisano
                
            };
            return novi;
        }

        public override string ToString()
        {
            return $"{Sifra} {Odrediste} {Destinacija} {VremePolaska} {VremeDolaska}";
        }
    }
}
