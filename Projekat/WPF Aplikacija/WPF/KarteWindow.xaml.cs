﻿using ConsoleAppFirst.Model;
using DrugiWPF.DAL;
using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for KarteWindow.xaml
    /// </summary>
    public partial class KarteWindow : Window
    {
        public enum PretragaPO { BROJLETA, PUTNIK, BIZNISSEDISTE, SEDISTE }

        ICollectionView view;
        public int korisnikID;
        public Korisnik korisnik;
        public KarteWindow(int id, Korisnik _korisnik)
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Karte);
            DGKarte.ItemsSource = view;
            DGKarte.IsSynchronizedWithCurrentItem = true;
            DGKarte.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            korisnikID = id;
            korisnik = _korisnik;
            //Vizualno brisanje sa liste kad se obrise
            view.Filter = CustomFilter;
            //Dodaj u combo box za pretragu
            NapuniComboBox();
            //refresh db
            KartaDAL.UcitajKarte();
        }

        private bool CustomFilter(object obj)
        {

            Karta k = obj as Karta;
            switch (CBPretragaPo.SelectedValue)
            {
                case PretragaPO.BIZNISSEDISTE:
                    return k.ID.Equals(korisnikID) && k.BiznisSediste.ToString().ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.BROJLETA:
                    return k.ID.Equals(korisnikID) && k.BrojLeta.ToString().ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.PUTNIK:
                    return k.ID.Equals(korisnikID) && k.Putnik.ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.SEDISTE:
                    return k.ID.Equals(korisnikID) && k.Sediste.ToString().ToLower().Contains(TBPretraga.Text.ToLower());
                default:
                    return k.ID.Equals(korisnikID);
            }

        }
        private void NapuniComboBox()
        {
            CBPretragaPo.ItemsSource = Enum.GetValues(typeof(PretragaPO));
        }

        private void TBPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void DGKarte_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (!korisnik.TipKorisnika.Equals(ETipKorisnika.ADMINISTRATOR))
            {
                if (e.Column.Header.ToString() == "ID" || e.Column.Header.ToString() == "Obrisano")
                {
                    e.Cancel = true;
                }
            }

        }
    }
}
