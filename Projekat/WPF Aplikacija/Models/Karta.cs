﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.Models
{
    public class Karta : INotifyPropertyChanged, ICloneable
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; OnPropertyChanged("ID"); }
        }

        private int brojLeta;

        public int BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; OnPropertyChanged("BrojLeta"); }
        }

        private string sediste;

        public string Sediste
        {
            get { return sediste; }
            set { sediste = value; OnPropertyChanged("Sediste"); }
        }

        private string putnik;

        public string Putnik
        {
            get { return putnik; }
            set { putnik = value; OnPropertyChanged("Putnik"); }
        }

        private bool biznisSediste;

        public bool BiznisSediste
        {
            get { return biznisSediste; }
            set { biznisSediste = value; OnPropertyChanged("BiznisSediste"); }
        }

        private int ukupnaCena;

        public int UkupnaCena
        {
            get { return ukupnaCena; }
            set { ukupnaCena = value; OnPropertyChanged("UkupnaCena"); }
        }
        private bool povratnaKarta;

        public bool PovratnaKarta
        {
            get { return povratnaKarta; }
            set { povratnaKarta = value; OnPropertyChanged("PovratnaKarta"); }
        }


        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"ID {id}, Broj leta {brojLeta}, Putnik {putnik}";
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public object Clone()
        {
            Karta novi = new Karta
            {
                ID = this.id,
                BiznisSediste = this.biznisSediste,
                BrojLeta = this.brojLeta,
                Putnik = this.putnik,
                Sediste = this.sediste,
                UkupnaCena = this.ukupnaCena,
                PovratnaKarta = this.povratnaKarta
            };
            return novi;
        }
    }
}
