﻿using ConsoleAppFirst.Model;
using DrugiWPF.BLL;
using DrugiWPF.DAL;
using DrugiWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DrugiWPF.Util
{
    class Aplikacija
    {
        public ObservableCollection<Sediste> Sedista { get; set; }
        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Aerodrom> Aerodromi { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public ObservableCollection<Avion> Avioni { get; set; }
        public ObservableCollection<Aviokompanija> Aviokompanije { get; set; }
        public ObservableCollection<Karta> Karte { get; set; }

        public const string CONNECTION_STRING = @"Server=localhost\SQLEXPRESS;
                                                    Initial Catalog=AvioKompanija;
                                                    Trusted_Connection=True;";
        public String UlogovanKorisnik;

        private Aplikacija()
        {
            UlogovanKorisnik = String.Empty;

            Sedista = new ObservableCollection<Sediste>();
            Korisnici = new ObservableCollection<Korisnik>();
            Aerodromi = new ObservableCollection<Aerodrom>();
            Letovi = new ObservableCollection<Let>();
            Avioni = new ObservableCollection<Avion>();
            Aviokompanije = new ObservableCollection<Aviokompanija>();
            Karte = new ObservableCollection<Karta>();
        }


        private static Aplikacija _instance = null;

        public static Aplikacija Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Aplikacija();
                return _instance;
            }
        }
        
    }
}