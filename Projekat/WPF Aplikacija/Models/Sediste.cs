﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.Models
{
    public class Sediste : INotifyPropertyChanged, ICloneable
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; OnPropertyChanged("ID"); }
        }

        private string zauzetaSedista;

        public string ZauzetaSedista
        {
            get { return zauzetaSedista; }
            set { zauzetaSedista = value; OnPropertyChanged("ZauzetaSedista"); }
        }

        private string slobodnaSedista;

        public string SlobodnaSedista
        {
            get { return slobodnaSedista; }
            set { slobodnaSedista = value; OnPropertyChanged("SlobodnaSedista"); }
        }

        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        private bool biznisSediste;

        public bool BiznisSediste
        {
            get { return biznisSediste; }
            set { biznisSediste = value; OnPropertyChanged("BiznisSediste"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return $"ID {ID}, Slobodna {SlobodnaSedista}, Zauzeta {ZauzetaSedista}, Biznis sediste {biznisSediste}";
        }

        public object Clone()
        {
            Sediste novi = new Sediste
            {
                ID = this.id,
                SlobodnaSedista = this.slobodnaSedista,
                ZauzetaSedista = this.zauzetaSedista,
                BiznisSediste = this.biznisSediste
            };
            return novi;
        }
    }
}
