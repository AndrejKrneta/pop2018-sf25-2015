﻿using ConsoleAppFirst.Model;
using DrugiWPF.DAL;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for KorisniciEditWindow.xaml
    /// </summary>
    public partial class KorisniciEditWindow : Window
    {
        public enum Opcija { DODAJ, IZMENA }
        Korisnik korisnik;
        Opcija opcija;

        public KorisniciEditWindow()
        {
            InitializeComponent();
        }

        public KorisniciEditWindow(Korisnik korisnik, Opcija opcija = Opcija.DODAJ)
        {
            InitializeComponent();
            NapuniComboBox();

            this.korisnik = korisnik;
            this.opcija = opcija;
            this.DataContext = korisnik;
            
            if (opcija.Equals(Opcija.IZMENA))
            {
                TXTKorisnikoIme.IsEnabled = false;
            }
        }

        public KorisniciEditWindow(Korisnik korisnik)
        {
            InitializeComponent();
            NapuniComboBox();

            this.korisnik = korisnik;
            this.DataContext = korisnik;

            TXTKorisnikoIme.IsEnabled = false;
            CBTipKorisnika.IsEnabled = false;
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(Opcija.DODAJ))
            {
                korisnik.TipKorisnika = (ETipKorisnika)Enum.Parse(typeof(ETipKorisnika), CBTipKorisnika.Text);
                //dodaj u db
                KorisnikDAL.DodajKorisnika(korisnik);
                //dodaj u model
                Aplikacija.Instance.Korisnici.Add(korisnik);
                //ID FIX refresh kad doda
                KorisnikDAL.UcitajKorisnike();
            }else if (opcija.Equals(Opcija.IZMENA))
            {
                KorisnikDAL.IzmenaKorisnika(korisnik);
                //refresh list
                KorisnikDAL.UcitajKorisnike();
            }
            else
            {
                KorisnikDAL.IzmenaKorisnika(korisnik);
                //refresh list
                KorisnikDAL.UcitajKorisnike();
            }

        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private bool PostojiKorisnik(string kime)
        {
            if (kime != null)
            {
                foreach (var let in Aplikacija.Instance.Korisnici)
                {
                    if (korisnik.KorisnickoIme.Equals(kime))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void NapuniComboBox()
        {
            CBTipKorisnika.ItemsSource = Enum.GetValues(typeof(ETipKorisnika));
        }
    }
}
