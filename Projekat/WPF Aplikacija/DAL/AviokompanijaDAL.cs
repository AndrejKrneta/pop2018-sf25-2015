﻿using DrugiWPF.BLL;
using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.DAL.SQL
{
    public class AviokompanijaDAL
    {
        public static void UcitajAviokompanije()
        {
            Aplikacija.Instance.Aerodromi.Clear();

            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM AVIOKOMPANIJA";

                SqlDataAdapter daAdapter = new SqlDataAdapter();
                DataSet daAviokompanije = new DataSet();

                daAdapter.SelectCommand = command;
                daAdapter.Fill(daAviokompanije, "AVIOKOMPANIJA");

                foreach (DataRow row in daAviokompanije.Tables["AVIOKOMPANIJA"].Rows)
                {
                    Aviokompanija ak = new Aviokompanija();
                    {
                        ak.ID = (int)row["id"];
                        ak.Sifra = (string)row["sifra"];
                        ak.LetID = (int)row["letid"];
                        ak.Obrisano = (bool)row["obrisano"];
                        try
                        {
                            ak.Let = LetBLL.GetById(ak.LetID);
                        }
                        catch (Exception e) { Console.WriteLine(e); }
                        Aplikacija.Instance.Aviokompanije.Add(ak);
                    }
                }
            }
        }

        public static void IzmenaAvikompanije(Aviokompanija a)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE AVIOKOMPANIJA 
                                        SET id=@id, sifra=@sifra, letid=@letid, obrisano=@obrisano 
                                        WHERE id=@id";

                command.Parameters.Add(new SqlParameter("@sifra", a.ID));
                command.Parameters.Add(new SqlParameter("@sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@letid", a.LetID));
                command.Parameters.Add(new SqlParameter("@obrisano", a.Obrisano));

                command.ExecuteNonQuery();
            }
        }
    }
}
