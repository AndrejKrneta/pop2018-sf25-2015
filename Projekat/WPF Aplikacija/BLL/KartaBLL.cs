﻿using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.BLL
{
    class KartaBLL
    {
        public static Karta GetById(int id)
        {
            foreach (var k in Aplikacija.Instance.Karte)
            {
                if (k.ID == id)
                {
                    return k;
                }
            }
            return null;
        }
        
    }
}
