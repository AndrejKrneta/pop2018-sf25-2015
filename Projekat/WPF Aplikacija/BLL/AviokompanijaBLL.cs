﻿using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.BLL
{
    class AviokompanijaBLL
    {
        public static Aviokompanija GetById(int id)
        {
            foreach (var aviokompanija in Aplikacija.Instance.Aviokompanije)
            {
                if (aviokompanija.ID == id)
                {
                    return aviokompanija;
                }
            }
            return null;
        }
    }
}
