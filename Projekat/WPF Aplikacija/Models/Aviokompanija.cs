﻿using DrugiWPF.BLL;
using DrugiWPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.Models
{
    public class Aviokompanija : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private string sifra;
        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; }
        }

        private bool obrisano;

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; }
        }
        private int letID;

        public int LetID
        {
            get { return letID; }
            set { letID = value; OnPropertyChanged("LetID"); }
        }

        private Let let;

        public Let Let
        {
            get
            {
                if (let == null)
                {
                    let = LetBLL.GetById(LetID);
                }
                return let;
            }
            set
            {
                let = value;
                LetID = let.ID;
                OnPropertyChanged("Let");
            }
        }

        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"Sifra {Sifra}";
        }


        public object Clone()
        {
            Aviokompanija novi = new Aviokompanija()
            {
                id = this.id,
                Sifra = this.sifra,
                LetID = this.letID,
                Let = this.let,
                Obrisano = this.obrisano
            };
            return novi;
        }
    }
}
