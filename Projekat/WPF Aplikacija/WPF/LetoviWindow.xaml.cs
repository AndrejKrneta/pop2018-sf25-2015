﻿using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DrugiWPF.WPF;
using System.ComponentModel;
using DrugiWPF.DAL;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for LetoviWindow.xaml
    /// </summary>
    public partial class LetoviWindow : Window
    {
        public enum PretragaPO { BROJLETA, CENA, ODREDISTE, DESTINACIJA, VREMEPOLASKA, VREMEDOLASKA}

        ICollectionView view;

        public LetoviWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Letovi);

            DGLetovi.ItemsSource = view;
            DGLetovi.IsSynchronizedWithCurrentItem = true;
            DGLetovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            
            view.Filter = CustomFilter;

            NapuniComboBox();
        }

        private bool CustomFilter(object obj)
        {
            Let let = obj as Let;
            switch (CBPretragaPo.SelectedValue)
            {

                case PretragaPO.BROJLETA:
                    return !let.Obrisano && let.Sifra.ToString().ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.CENA:
                    return !let.Obrisano && let.CenaKarte.ToString().ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.DESTINACIJA:
                    return !let.Obrisano && let.Destinacija.ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.ODREDISTE:
                    return !let.Obrisano && let.Odrediste.ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.VREMEPOLASKA:
                    return !let.Obrisano && let.VremePolaska.ToString().ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.VREMEDOLASKA:
                    return !let.Obrisano && let.VremeDolaska.ToString().ToLower().Contains(TBPretraga.Text.ToLower());
                default:
                    return !let.Obrisano;
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            LetoviEditWindow aew = new LetoviEditWindow(new Let(), LetoviEditWindow.Opcija.DODAJ);
            if (aew.ShowDialog() == true)
            {
                view.Refresh();
            }

        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {


            Let selektivanLet = DGLetovi.SelectedItem as Let;
            
            if (selektivanLet != null)
            {
                LetoviEditWindow lew = new LetoviEditWindow(selektivanLet, LetoviEditWindow.Opcija.IZMENA);
                if (lew.ShowDialog() == false)
                {
                    
                    view.Refresh();
                }
                view.Refresh();
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DGLetovi.SelectedItem;
            if (SelektovanLet(let))
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksSelektovanogLeta(let.Sifra.ToString());
                    if (indeks != -1)
                    {
                        //model izbrisi
                        Aplikacija.Instance.Letovi[indeks].Obrisano = true;
                        //db izbrisi
                        LetDAL.IzbrisiLet(let);
                    }
                        
                    view.Refresh();
                }
            }
        }

        private bool SelektovanLet(Let let)
        {
            if (let == null)
            {
                MessageBox.Show("Nije selektovan let");
                return false;
            }
            return true;
        }
        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private int IndeksSelektovanogLeta(String sifra)
        {
            var indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Letovi.Count; i++)
            {
                if (Aplikacija.Instance.Letovi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;

                }
            }
            return indeks;
        }

        private void NapuniComboBox()
        {
            CBPretragaPo.ItemsSource = Enum.GetValues(typeof(PretragaPO));
        }

        private void TBPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

    }
}
