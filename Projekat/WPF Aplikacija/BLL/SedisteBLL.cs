﻿using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.BLL
{
    class SedisteBLL
    {
        public static Sediste GetById(int id)
        {
            foreach (var sediste in Aplikacija.Instance.Sedista)
            {
                if (sediste.ID == id)
                {
                    return sediste;
                }
            }
            return null;
        }

        public static Sediste GetByIdBiznisSediste(int id)
        {
            foreach (var sediste in Aplikacija.Instance.Sedista)
            {
                if (sediste.ID == id && sediste.BiznisSediste)
                {
                    return sediste;
                }
            }
            return null;
        }
        public static string NamestiSedista(int brojSedista, int brBizSedsita)
        {
            string namestenaSedista = "";
            //t broj reda
            int t = 0;
            //b broj sedista
            int b = 0;
            for (int i = brBizSedsita+1; i <= brojSedista+brBizSedsita+1; i++)
            {
                b++;

                if (i%4 == 0)
                    t++;

                if (b > brBizSedsita+1)
                {
                    switch (i % 4)
                    {
                        case 0:
                            namestenaSedista += "A" + t + "|";
                            
                            break;
                        case 1:
                            namestenaSedista += "B" + t + "|";
                            
                            break;
                        case 2:
                            namestenaSedista += "C" + t + "|";
                            
                            break;
                        case 3:
                            namestenaSedista += "D" + t + "|";
                            
                            break;
                    }
                }

            }

            return namestenaSedista;
        }

        public static string NamestiBiznisSedista(int brojBiznisSedista)
        {
            string namestenaSedista = "";
            int t = 0;
            for (int i = 1; i <= brojBiznisSedista; i++)
            {
                if (i % 4 == 1)
                    t++;

                switch (i % 4)
                {
                    case 1:
                        namestenaSedista += "A" + t + "|";

                        break;
                    case 2:
                        namestenaSedista += "B" + t + "|";

                        break;
                    case 3:
                        namestenaSedista += "C" + t + "|";

                        break;
                    case 0:
                        namestenaSedista += "D" + t + "|";

                        break;
                }
            }

            return namestenaSedista;
        }

        public static List<string> ListaSvihSedista(Avion avion)
        {
            string stringLista = avion.Sediste.SlobodnaSedista;
            List<string> lista = new List<string>();
            lista = stringLista.Split('|').ToList();
            lista.RemoveAt(lista.Count-1);
            return lista;
        }

        public static List<string> ListaSvihBiznisSedista(Avion avion)
        {
            string stringLista = avion.BiznisSediste.SlobodnaSedista;
            List<string> lista = new List<string>();
            lista = stringLista.Split('|').ToList();
            lista.RemoveAt(lista.Count - 1);
            return lista;
        }

        public static bool PostojanostSedista(string imeSedista, List<string> listaSedista)
        {
            if (listaSedista.Contains(imeSedista))
            {
                return true;
            }
            return false;
        }

        public static Sediste SlobodnoNaZauzetoSediste(string nazivSediste, Sediste sediste)
        {
            sediste.SlobodnaSedista = sediste.SlobodnaSedista.Replace(nazivSediste + "|", "");
            sediste.ZauzetaSedista += nazivSediste;
            return sediste;
        }


    }
}
