﻿using DrugiWPF.Models;
using DrugiWPF.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.Models
{
    public class Avion : INotifyPropertyChanged, ICloneable
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; OnPropertyChanged("ID"); }
        }

        private string pilot;

        public string Pilot
        {
            get { return pilot; }
            set { pilot = value; OnPropertyChanged("Pilot"); }
        }

        private int brojLeta;

        public int BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; OnPropertyChanged("BrojLeta"); }
        }

        private string nazivAviokompanije;

        public string NazivAviokompanije
        {
            get { return nazivAviokompanije; }
            set { nazivAviokompanije = value; OnPropertyChanged("NazivAviokompanije"); }
        }

        private bool obrisano;

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }

        private int sedisteID;

        public int SedisteID
        {
            get { return sedisteID; }
            set { sedisteID = value; }
        }
        private int brojBiznisSedista;

        public int BrojBiznisSedista
        {
            get { return brojBiznisSedista; }
            set { brojBiznisSedista = value; OnPropertyChanged("BrojBiznisSedista"); }
        }

        private Sediste sediste;

        public Sediste Sediste
        {
            get
            {
                if (sediste == null)
                {
                     sediste = SedisteBLL.GetById(SedisteID);
                }
                return sediste;
            }
            set
            {
                sediste = value;
                SedisteID = sediste.ID;
                OnPropertyChanged("Sediste");
            }
        }

        private int biznisSedisteID;

        public int BiznisSedisteID
        {
            get { return biznisSedisteID; }
            set { biznisSedisteID = value; }
        }


        private Sediste biznisSediste;

        public Sediste  BiznisSediste
        {
            get
            {
                if (biznisSediste == null)
                {
                    if (SedisteBLL.GetById(BiznisSedisteID).BiznisSediste)
                        biznisSediste = SedisteBLL.GetById(BiznisSedisteID);

                }
                return biznisSediste;
            }
            set
            {
                biznisSediste = value;
                BiznisSedisteID = biznisSediste.ID;
                OnPropertyChanged("BiznisSediste");
            }
        }
        private int brojSedista;

        public int BrojSedista
        {
            get { return brojSedista; }
            set { brojSedista = value; OnPropertyChanged("BrojSedista"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"Pilot {Pilot},Biznis sediste ID {BiznisSedisteID}, Sediste ID {SedisteID}";
        }

        public object Clone()
        {
            Avion novi = new Avion
            {
                ID = this.id,
                Pilot = this.pilot,
                BrojLeta = this.brojLeta,
                SedisteID = this.sedisteID,
                Sediste = this.sediste,
                NazivAviokompanije = this.nazivAviokompanije,
                BrojSedista = this.brojSedista,
                BrojBiznisSedista = this.brojBiznisSedista,
                Obrisano = this.obrisano
            };
            return novi;
        }
    }
}
