﻿using ConsoleAppFirst.Model;
using DrugiWPF.DAL;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for KorisniciWindow.xaml
    /// </summary>
    public partial class KorisniciWindow : Window
    {
        public enum PretragaPO { IME, PREZIME, KIME, TKORISNIKA}

        ICollectionView view;
        public KorisniciWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Korisnici);
            DGKorisnici.ItemsSource = view;
            DGKorisnici.IsSynchronizedWithCurrentItem = true;
            DGKorisnici.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            
            //Vizualno brisanje sa liste kad se obrise
            view.Filter = CustomFilter;
            //Dodaj u combo box za pretragu
            NapuniComboBox();
        }

        private bool CustomFilter(object obj)
        {

            Korisnik korisnik = obj as Korisnik;
            switch (CBPretragaPo.SelectedValue)
            {
                case PretragaPO.IME:
                    return !korisnik.Obrisano && korisnik.Ime.ToLower().Contains(TBPretraga.Text.ToLower());

               case PretragaPO.KIME:
                    return !korisnik.Obrisano && korisnik.KorisnickoIme.ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.PREZIME:
                    return !korisnik.Obrisano && korisnik.Prezime.ToLower().Contains(TBPretraga.Text.ToLower());

                case PretragaPO.TKORISNIKA:
                    return !korisnik.Obrisano && korisnik.TipKorisnika.ToString().ToLower().Contains(TBPretraga.Text.ToLower());
                default:
                    return !korisnik.Obrisano;
            }

        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            KorisniciEditWindow kew = new KorisniciEditWindow(new Korisnik(), KorisniciEditWindow.Opcija.DODAJ);
            if (kew.ShowDialog() == true)
            {
                
                view.Refresh();
            }
        }
        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        
        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = (Korisnik)DGKorisnici.SelectedItem;
            if (SelektovanKorisnik(korisnik))
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksSelektovanogKorisnika(korisnik.KorisnickoIme);
                    if(indeks != -1)
                    {
                        //model izbrisi
                        Aplikacija.Instance.Korisnici[indeks].Obrisano = true;
                        //db izbrisi
                        KorisnikDAL.IzbrisiKorisnika(korisnik);
                    }
                    view.Refresh();
                }
            }

        }

        private int IndeksSelektovanogKorisnika(String kime)
        {
            var indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Korisnici.Count; i++)
            {
                if (Aplikacija.Instance.Korisnici[i].KorisnickoIme.Equals(kime))
                {
                    indeks = i;
                    break;

                }
            }
            return indeks;
        }

        private bool SelektovanKorisnik(Korisnik korisnik)
        {
            if (korisnik == null)
            {
                MessageBox.Show("Nije selektovan korisnik");
                return false;
            }
            return true;
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = (Korisnik)DGKorisnici.SelectedItem;
            if (SelektovanKorisnik(korisnik))
            {
                Korisnik stari = korisnik.Clone() as Korisnik;
                KorisniciEditWindow kew = new KorisniciEditWindow(korisnik, KorisniciEditWindow.Opcija.IZMENA);
                if (kew.ShowDialog() != true)
                {
                    int indeks = IndeksSelektovanogKorisnika(korisnik.KorisnickoIme);
                    Aplikacija.Instance.Korisnici[indeks] = stari;
                    view.Refresh();
                }
            }

        }

        private void NapuniComboBox()
        {
            CBPretragaPo.ItemsSource = Enum.GetValues(typeof(PretragaPO));
        }

        private void TBPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
