﻿using ConsoleAppFirst.Model;
using DrugiWPF.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DrugiWPF.Models;
using DrugiWPF.Util;
using DrugiWPF.BLL;

namespace DrugiWPF.WPF
{
    /// <summary>
    /// Interaction logic for AvionEditWindow.xaml
    /// </summary>
    public partial class AvionEditWindow : Window
    {
        public enum Opcija { DODAJ, IZMENA }
        Avion avion;
        Opcija opcija;


        public AvionEditWindow()
        {
            InitializeComponent();
        }

        public AvionEditWindow(Avion avion, Opcija opcija = Opcija.DODAJ)
        {
            InitializeComponent();
            this.avion = avion;
            this.opcija = opcija;
            this.DataContext = avion;

            if (opcija.Equals(Opcija.IZMENA))
            {
                TXTBrojLeta.IsEnabled = false;
            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(TXTPilot.Text) && TXTPilot.Text.Length < 5)
            {
                MessageBox.Show("Molimo vas unestie ime i prezime pilota");
            }
            else if (!int.TryParse(TXTBrojLeta.Text, out int result) || result < 100 || PostojiAvion(result))
            {
                MessageBox.Show("Molimo vas da broj leta bude veci od dvocifrenog broja ili unesite broj umesto slova ili broj leta vec postoji");
            }
            else if(String.IsNullOrEmpty(TXTNazivAviokompanije.Text))
            {
                MessageBox.Show("Molimo vas unestie naziv aviokompanije");
            }
            else if (!int.TryParse(TXTUkupanBrojSedista.Text, out int result1) || !(result1 <= 72))
            {
                MessageBox.Show("Maksimalan broj sedista je 72 ili unosite slova umesto brojeva");
            }
            else if (!int.TryParse(TXTUkupanBrojBiznisSedista.Text,out int reuslt2) || !(reuslt2 < result1))
            {
                MessageBox.Show("Ukupan broj biznis sedista ne smije biti veci od ukupnog broja sedista ili unosite slova umesto brojeva");
            }
            else
            {
                this.DialogResult = true;
                if (opcija.Equals(Opcija.DODAJ))
                {

                    Sediste s1 = new Sediste()
                    {
                        ID = Aplikacija.Instance.Avioni.Count + 1,
                        SlobodnaSedista = SedisteBLL.NamestiSedista(avion.BrojSedista, avion.BrojBiznisSedista),
                        ZauzetaSedista = "",
                        BiznisSediste = false
                    };
                    Sediste s2 = new Sediste()
                    {
                        ID = Aplikacija.Instance.Avioni.Count + 1,
                        SlobodnaSedista = SedisteBLL.NamestiBiznisSedista(avion.BrojBiznisSedista),
                        ZauzetaSedista = "",
                        BiznisSediste = true
                    };

                    //Namjestanje sedista aviona
                    avion.Sediste = s1;
                    avion.BiznisSediste = s2;
                    //Dodaj sedista u db
                    SedisteDAL.DodajSediste(s1);
                    SedisteDAL.DodajSediste(s2);
                    //dodaj u db
                    AvionDAL.DodajAvion(avion);
                    //dodaj u model
                    Aplikacija.Instance.Avioni.Add(avion);
                    //ID FIX refresh kad doda
                    AvionDAL.UcitajAvione();
                }
                if (opcija.Equals(Opcija.IZMENA))
                {
                    //izmena
                }
            }

        }
        private bool PostojiAvion(int bleta)
        {
            if (bleta != null)
            {
                foreach (var avion in Aplikacija.Instance.Avioni)
                {
                    if (avion.BrojLeta.Equals(bleta) && !avion.Obrisano)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
