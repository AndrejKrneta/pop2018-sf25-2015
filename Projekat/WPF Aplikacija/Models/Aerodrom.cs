﻿using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppFirst.Model
{
    public class Aerodrom : INotifyPropertyChanged, ICloneable
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; OnPropertyChanged("ID"); }
        }


        private string sifra;
        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private string grad;
        public string Grad
        {
            get { return grad; }
            set { grad = value; OnPropertyChanged("Grad"); }
        }

        private string naziv;
        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; OnPropertyChanged("Naziv"); }
        }

        private bool obrisano;
        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        public override string ToString()
        {
            return $"Sifra {Sifra}, Naziv {Naziv}, Grad {Grad}";
        }



        public object Clone()
        {
            Aerodrom novi = new Aerodrom
            {
                ID = this.id,
                Sifra = this.sifra,
                Grad = this.grad,
                Naziv = this.naziv,
                Obrisano = this.obrisano
            };
            return novi;
        }

    }

}
