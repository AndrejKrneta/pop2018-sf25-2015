﻿using DrugiWPF.Models;
using DrugiWPF.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugiWPF.DAL
{
    public class SedisteDAL
    {
        public static void UcitajSedista()
        {
            Aplikacija.Instance.Sedista.Clear();

            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT * FROM SEDISTE";

                SqlDataAdapter daAdapter = new SqlDataAdapter();
                DataSet daSedista = new DataSet();

                daAdapter.SelectCommand = command;
                daAdapter.Fill(daSedista, "SEDISTE");

                foreach (DataRow row in daSedista.Tables["SEDISTE"].Rows)
                {
                    Sediste s = new Sediste();
                    {
                        s.ID = (int)row["id"];
                        s.SlobodnaSedista = (string)row["slobodnasedista"];
                        s.ZauzetaSedista = (string)row["zauzetasedista"];
                        s.BiznisSediste = (bool)row["biznissediste"];

                        Aplikacija.Instance.Sedista.Add(s);
                    }
                }
            }
        }
        public static void DodajSediste(Sediste s)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO SEDISTE(id, slobodnasedista, zauzetasedista, biznissediste) 
                                        VALUES (@id, @slobodnasedista, @zauzetasedista, @biznissediste)";


                command.Parameters.Add(new SqlParameter("@id", s.ID));
                command.Parameters.Add(new SqlParameter("@slobodnasedista", s.SlobodnaSedista));
                command.Parameters.Add(new SqlParameter("@zauzetasedista", s.ZauzetaSedista));
                command.Parameters.Add(new SqlParameter("@biznissediste", s.BiznisSediste));

                command.ExecuteNonQuery();
            }
        }

        public static void IzmenaSedista(Sediste s)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE SEDISTE 
                                        SET id=@id, slobodnasedista=@slobodnasedista, zauzetasedista=@zauzetasedista, biznissediste=@biznissediste
                                        WHERE id=@id AND biznissediste=@biznissediste";

                command.Parameters.Add(new SqlParameter("@id", s.ID));
                command.Parameters.Add(new SqlParameter("@slobodnasedista", s.SlobodnaSedista));
                command.Parameters.Add(new SqlParameter("@zauzetasedista", s.ZauzetaSedista));
                command.Parameters.Add(new SqlParameter("@biznissediste", s.BiznisSediste));

                command.ExecuteNonQuery();
            }
        }

        public static void UpadteSedista(Sediste s)
        {
            using (SqlConnection connection = new SqlConnection(Aplikacija.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE SEDISTE 
                                        SET slobodnasedista=@slobodnasedista, zauzetasedista=@zauzetasedista
                                        WHERE id=@id AND biznissediste=@biznissediste";

                command.Parameters.Add(new SqlParameter("@id", s.ID));
                command.Parameters.Add(new SqlParameter("@slobodnasedista", s.SlobodnaSedista));
                command.Parameters.Add(new SqlParameter("@zauzetasedista", s.ZauzetaSedista));
                command.Parameters.Add(new SqlParameter("@biznissediste", s.BiznisSediste));

                command.ExecuteNonQuery();
            }
        }
    }
}
